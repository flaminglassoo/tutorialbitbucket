package com.tutorialbitbucket.android.Activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.tutorialbitbucket.android.R;
import com.tutorialbitbucket.android.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        clickListener();
    }

    private void clickListener() {
        binding.fab.setOnClickListener(v -> Toast.makeText(this, "Floating Action Button was clicked", Toast.LENGTH_SHORT).show());
        binding.textView.setOnClickListener(view -> Toast.makeText(this, "Textview was clicked", Toast.LENGTH_SHORT).show());
    }

}
